package game;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JPanel;

class Square extends JPanel
{
	private JLabel label = new JLabel();
	/**
	 * obrazek pola
	 */
	private BufferedImage image;
	
	public Square()
	{
		setBackground(Color.white);
		setPreferredSize(new Dimension(32, 32));
		add(label);
	}
	
	/**
	 * wybieranie obrazka na podstawie nazwy pliku .png w folderze data/
	 * @param direction
	 * @return
	 */
	public Square setIcon(String direction) {
			
		try{
			image = null;
		    image = ImageIO.read(new File(direction));
		}
		catch (IOException ex)
		{
		    System.exit(0);
		}
        return this;
    }
	
	@Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, label);  
    }

	private static final long serialVersionUID = -4543700669613623212L;
}