package game;

public enum MessageType {
	WELCOME,
	MESSAGE,
	MOVE,
	VALID_MOVE,
	NOTIFY,
	MARK,
	VICTORY,
	DEFEAT,
	REMIS,
	SCORED,
	OPPONENT_SCORED,
	BUTTON1,
	BUTTON2,
	SHIFT,
	QUIT;
}
