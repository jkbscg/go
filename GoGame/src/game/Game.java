package game;
import java.io.IOException;

class Game
{
	/**
	 * obecna plansza i jej kopie potrzebne do egzekwowania reguł gry, takich jak 
	 * zakaz wracania do stanu sprzed poprzedniego ruchu, czy zakaz samobójstwa
	 */
	private Field[][] very_old_board;
	private Field[][] old_board;
	private Field[][] board;
	public int size;
	/**
	 * ilość ominiętych kolejek w rzędzie
	 */
	private int skipped = 0;
	/**
	 * Czy gra jest w trybie oznaczania pionów?
	 */
	private boolean MarkingMode = false;
	
	/**
	 * referencja do obecnego gracza
	 */
	Player currentPlayer;
	
	/**
	 * jeśli ruch jest dozwolony, wykonuje go i wykonuje konieczne czynności poboczne
	 * (naliczenie wyniku, zmiana gracza, zaznaczenie, że kolejka nie została opuszczona,
	 * odznaczenie akceptacji zaznaczenia przez graczy)
	 * @param location
	 * @param player
	 * @return
	 */
	public synchronized boolean legalMove(int location, Player player)
	{
		int a = location/size, b = location%size;
		if (!MarkingMode)
		{
			if (player == currentPlayer && Field.isEmpty(board[a][b]))
			{
				board[a][b].state = currentPlayer.mark;		
				refactor(a, b);

				if (count(board, player.mark) <= count(old_board, player.mark) || !differentBoards())
				{
					makeCopy(old_board, board);
					return false;
				}
				else
				{
					int result = count(old_board, player.opponent.mark) - count(board, player.opponent.mark);
					player.scored(result, true);
					player.opponent.scored(result,  false);
				}

				for (int i=0; i<size; ++i)
				{
					for (int j=0; j<size; ++j)
					{
						if (board[i][j].state != old_board[i][j].state)
						{
							notify(currentPlayer);
							notify(currentPlayer.opponent);
						}
					}
				}
				refactor(a, b);
				makeCopy(old_board, very_old_board);
				makeCopy(board, old_board);
				currentPlayer = currentPlayer.opponent;
				skipped = 0;
				return true;
			}
			return false;
		}
		else
		{
			uncheck();
			mark(a, b, FieldState.getTerritory(player.mark));
			notify(player);
			notify(player.opponent);
			player.accepting = false;
			player.opponent.accepting = false;
			System.out.println("marked...");
			return true;
		}
	}
	
	/**
	 * usuwa zduszone piony po wejściu pionu na określone miejsce
	 * @param a	współrzędna x piona
	 * @param b współrzędna y piona
	 */
	private void refactor(int a, int b)
	{
		uncheck();
		if(a>0)
		{
			if (!hasBreathe(a-1, b, board[a-1][b].state)) 
			{
				uncheck();
				remove(a-1, b, board[a-1][b].state);
			}
			uncheck();
		}
		if (a<size-1)
		{
			if (!hasBreathe(a+1, b, board[a+1][b].state))
			{
				uncheck();
				remove(a+1, b, board[a+1][b].state);
			}
			uncheck();
		}
		if (b>0)
		{
			if (!hasBreathe(a, b-1, board[a][b-1].state)) 
			{
				uncheck();
				remove(a, b-1, board[a][b-1].state);
			}
			uncheck();
		}
		if (b<size-1)
		{
			if (!hasBreathe(a, b+1, board[a][b+1].state)) 
			{
				uncheck();
				remove(a, b+1, board[a][b+1].state);
			}
			uncheck();
		}
		if (!hasBreathe(a, b, board[a][b].state)) 
		{
			uncheck();
			remove(a, b, board[a][b].state);
		}
		uncheck();
	}
	
	/**
	 * Sprawdza, czy pion w położeniu ma oddech:
	 * Pion przy boku nie ma oddechu od strony boku planszy.
	 * Pion przy pionie przeciwnika nie ma od jego strony oddechu.
	 * pion ma oddech wtedy i tylko wtedy, gdy któreś sąsiednie pole ma oddech.
	 * @param i współrzędna x pola
	 * @param j współrzędna y pola
	 * @param fs
	 * @return
	 */
	private boolean hasBreathe(int i, int j, FieldState fs)
	{
		if ( i<0 || j<0 || i>=size || j>= size )
			return false;
		if (Field.isEmpty(board[i][j]))
			return true;
		if (board[i][j].state == FieldState.getOpponent(fs))
		{
			return false;
		}
		if (board[i][j].checked)
			return false;
		board[i][j].markChecked();
		return 	hasBreathe(i-1,j,fs) || hasBreathe(i+1,j,fs) || hasBreathe(i,j-1,fs) || hasBreathe(i,j+1,fs);
	}
	
	/**
	 * usuwanie pionów z określonego pola i wszystkich sąsiadujących o tym samym kolorze
	 * @param i współrzędna x pola
	 * @param j współrzędna y pola
	 * @param fs kolor usuwanych pionów
	 */
	private void remove(int i, int j, FieldState fs)
	{
		if ( i<0 || j<0 || i>=size || j>= size )
			return;
		if (board[i][j].state == fs && !board[i][j].checked)
		{
//			System.out.println("usuwam...");
			board[i][j].state = FieldState.EMPTY;
			board[i][j].markChecked();
			remove(i-1,j,fs);
			remove(i+1,j,fs);
			remove(i,j-1,fs);
			remove(i,j+1,fs);
		}
	}
	
	/**
	 * Oznaczanie terytorium przez gracza
	 * @param a współrzędna x oznaczanego pola
	 * @param b współrzędna y oznaczanego pola
	 * @param fs stan, którym oznaczane są pola
	 */
	private void mark(int a, int b, FieldState fs)
	{
		if (a<0 || a>=size || b<0 || b>=size)
			return;
		if (FieldState.isEmpty(board[a][b].state) && !board[a][b].checked)
		{
			board[a][b].state = fs;
			board[a][b].markChecked();
			mark(a-1,b,fs);
			mark(a+1,b,fs);
			mark(a,b-1,fs);
			mark(a,b+1,fs);
		}
		else
		{
			board[a][b].markChecked();
		}
	}
	
	/**
	 * usuwanie wszystkich zaznaczonych terytoriów
	 */
	private void unmark()
	{
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
				if (FieldState.isEmpty(board[i][j].state))
					board[i][j].state = FieldState.EMPTY;
		notify( currentPlayer );
		notify( currentPlayer.opponent );
	}
	
	/**
	 * Zarządzanie pauzą; wchodzenie w tryb oznaczania lub naliczanie opuszczonej kolejki,
	 * ewentualnie wychodzenie z trybu oznaczania
	 * @param player gracz pauzujący
	 */
	public void pause(Player player)
	{
		if (!MarkingMode && player == currentPlayer)
		{
			try {
				player.opponent.obj_output.writeObject(new Message(MessageType.MESSAGE, "Enemy skipped"));
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (skipped==1)
			{
				System.out.println("pausing...");
				MarkingMode = true;
				player.shift();
				player.opponent.shift();
				skipped = 0;
			}
			else
			{
				++skipped;
				currentPlayer = currentPlayer.opponent;
			}
		}
		else if (MarkingMode)
		{
			makeCopy(board, old_board);
			unmark();
			MarkingMode = false;
			player.shift();
			player.opponent.shift();
//			notify(player);
//			notify(player.opponent);
			currentPlayer = player.opponent;
			skipped = 0;
			System.out.println("marking mode exited...");
		}
	}
	
	/**
	 * odznaczanie zaznaczenia 
	 */
	private void uncheck()
	{
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
				board[i][j].checked = false;
	}
	
	/**
	 * deep copy planszy
	 * @param from plansza źródłowa
	 * @param to plansza docelowa
	 */
	private void makeCopy(Field[][] from, Field[][] to)
	{
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
				to[i][j] = new Field(from[i][j]);
	}
	
	/**
	 * głębokie sprawdzanie, czy istnieje różnica pomiędzy obecną i poprzednią planszą
	 * @return
	 */
	private boolean differentBoards()
	{
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
				if (board[i][j].state!=very_old_board[i][j].state)
				{
//					System.out.println(x);
					return true;
				}
		return false;
	}
	
	/**
	 * liczenie pół o określonym stanie
	 * @param board plansza do przeszukania
	 * @param fs stan pola do odnalezienia
	 * @return
	 */
	private int count(Field[][]board, FieldState fs)
	{
		int res = 0;
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
				if (board[i][j].state == fs)
					++res;
		return res;
	}
	
	/**
	 * powiadomienie gracza o zmianie stanu pola
	 * @param player
	 */
	private void notify(Player player)
	{
		for (int i=0; i<size; ++i)
		{
			for (int j=0; j<size; ++j)
			{
				if (board[i][j].state != old_board[i][j].state)
				{
					player.notify(size*i+j,board[i][j].state);
				}
			}
		}
	}
	
	/**
	 * zaraządzanie prawym przyciskiem gracza - kończenie gry
	 * @param player gracz kończący
	 */
	public void endGame(Player player)
	{
		if (!MarkingMode)
		{
			try {
				player.opponent.obj_output.writeObject(new Message(MessageType.VICTORY, "; enemy surrendered"));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if (player.accepting && player.opponent.accepting) //  if (player.opponent.accepting())
		{
			int n;
			n = count(board, FieldState.getTerritory(player.mark));
			player.scored(n, true);
			player.opponent.scored(n, false);
			n = count(board, FieldState.getTerritory(player.opponent.mark));
			player.scored(n, false);
			player.opponent.scored(n, true);
			try{
				player.obj_output.writeObject(new Message(MessageType.SHIFT, 1));
				player.opponent.obj_output.writeObject(new Message(MessageType.SHIFT,1));
				if (player.result > player.opponent.result)
				{
					player.obj_output.writeObject(new Message(MessageType.VICTORY));
					player.opponent.obj_output.writeObject(new Message(MessageType.DEFEAT));
				}
				else if (player.result < player.opponent.result)
				{
					player.opponent.obj_output.writeObject(new Message(MessageType.VICTORY));
					player.obj_output.writeObject(new Message(MessageType.DEFEAT));
				}
				else
				{
					player.opponent.obj_output.writeObject(new Message(MessageType.REMIS));
					player.obj_output.writeObject(new Message(MessageType.REMIS));
				}
			}
			catch (Exception xD)
			{
				;
			}
		}
	}
	
	public Game(int size)
	{
		board = new Field[size][size]; 
		old_board = new Field[size][size];
		very_old_board = new Field[size][size]; 
		this.size = size;
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
			{
				board[i][j] = new Field();
				old_board[i][j] = new Field(); 
				very_old_board[i][j] = new Field(); 
			}	
	}
}
