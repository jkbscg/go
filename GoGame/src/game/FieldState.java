package game;
public enum FieldState
{	
	EMPTY, 
	WHITE, 
	BLACK, 
	WHITE_TERR, 
	BLACK_TERR,
	DUMMY;
	
	/**
	 * Stan terytorium gracza odpowiadający jego kolorowi
	 * @param fs
	 * @return
	 */
	public static FieldState getTerritory(FieldState fs)
	{
		switch(fs)
		{
		case BLACK:
			return BLACK_TERR;
		case WHITE:
			return WHITE_TERR;
		default:
			return DUMMY;
		}
	}
	
	/**
	 * Jaki jest stan pól zajętych przez przeciwnika?
	 * @param fs Flaga gracza, o którego przeciwniku należy wyszukać informacje
	 * @return
	 */
	public static FieldState getOpponent(FieldState fs)
	{
		if (fs == FieldState.WHITE)
		{
			return FieldState.BLACK;
		}
		else if (fs == FieldState.BLACK)
		{
			return FieldState.WHITE;
		}
		else
		{
			return FieldState.DUMMY;
		}
	}
	
	/**
	 * Przedstawienie stanu w formie stringa
	 * @param fs
	 * @return
	 */
	public static String toString(FieldState fs)
	{
		switch(fs)
		{
		case BLACK:
			return "BLACK";
		case WHITE:
			return "WHITE";
		case WHITE_TERR:
			return "WHITE_TERR";
		case BLACK_TERR:
			return "BLACK_TERR";
		default:
			return "EMPTY";
		}
	}
	
	/**
	 * Czy nie ma tu pionu?
	 * @param fs stan pola
	 * @return
	 */
	public static boolean isEmpty(FieldState fs)
	{
		return fs == EMPTY || fs == WHITE_TERR || fs == BLACK_TERR;
	}
}
