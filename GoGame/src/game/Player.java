package game;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JOptionPane;

class Player extends Thread
{
	public FieldState mark;
	Player opponent;
	Socket socket;
	BufferedReader input;
	PrintWriter output;
	ObjectOutputStream obj_output;
	ObjectInputStream obj_input;
	public double result;
	public final static double starting = 6.5;
	public boolean accepting = false;
	
	Game game;
	
	public Player() {}

	public Player(Socket socket)
	{
		this.socket = socket;
		try{
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new PrintWriter(socket.getOutputStream(), true);
			obj_output = new ObjectOutputStream(socket.getOutputStream());
			obj_input = new ObjectInputStream(socket.getInputStream());
			obj_output.writeObject(new Message(MessageType.MESSAGE, "Waiting for opponent to connect"));
		}
		catch (IOException xD)
		{
			System.out.println("Player died: "+xD);
		}
	}
	
	public void setMark(FieldState mark) throws IOException
	{
		this.mark = mark;
		obj_output.writeObject(new Message(MessageType.WELCOME, 0, mark));
	}

	public void setOpponent(Player opponent)
	{
		this.opponent = opponent;
	}
	
	public void setGame(Game game)
	{
		this.game = game;
	}

	/**
	 * zawiadomienie klineta o zmianie stanu pola
	 * @param location
	 * @param mark2
	 */
	public void notify(int location, FieldState mark2)
	{
		try{
			obj_output.writeObject(new Message(MessageType.NOTIFY, location, mark2));
		}
		catch (IOException xD)
		{
			System.out.println("nie tak...");
			System.exit(0);
		}
	}
	
	/**
	 * naliczenie wyniku sobie i przeciwnikowi
	 * @param value wynik
	 * @param notEnemy czy naliczamy sobie?
	 */
	public void scored(int value, boolean notEnemy)
	{
		if (notEnemy)
			result+=value;
		try {
			obj_output.writeObject(new Message(notEnemy ? MessageType.SCORED : MessageType.OPPONENT_SCORED, value));
		} catch (IOException e) {
			System.out.println("naliczanie wyniku nie dziala...");
			e.printStackTrace();
		}
	}
	
	public void shift()
	{
		accepting = false;
		try {
			obj_output.writeObject(new Message(MessageType.SHIFT, 0));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	/**
	 * pętla gry; wypisuje klientowi informacje od serwera przy użyciu serializacji
	 */
	public void run()
	{
		try{
//			System.out.println("Writing about connections...");
			obj_output.writeObject(new Message(MessageType.MESSAGE, "All players connected"));
			if (mark == FieldState.BLACK)
				obj_output.writeObject(new Message(MessageType.MESSAGE, "your move"));
			else if (mark == FieldState.WHITE)
				result = starting;
//			System.out.println("Written about connections...");

			for(;;)
			{
				Message message = (Message) obj_input.readObject();
				if (message.type == MessageType.MOVE)
				{
					int location = message.n;
					if (game.legalMove(location, this))
					{
						obj_output.writeObject(new Message(MessageType.VALID_MOVE));
						opponent.obj_output.writeObject(new Message(MessageType.MESSAGE, "your move"));
					}
					else
					{
						obj_output.writeObject(new Message(MessageType.MESSAGE));
					}
				}
				else if (message.type == MessageType.BUTTON1)
				{
					game.pause(this);
				}
				else if (message.type == MessageType.BUTTON2)
				{
					accepting = true;
					game.endGame(this);
				}
				else if (message.type == MessageType.QUIT)
				{
					try {
						finalize();
					} catch (Throwable e) {
						e.printStackTrace();
					}
					return;
				}
			}
		}
		catch (IOException xD)
		{
			System.out.println("139. Player died: "+xD);
		}
		catch (ClassNotFoundException xD)
		{
			System.out.println("Coś poszło nie tak...");
		}
		finally
		{
			try{
				socket.close();
			}
			catch (IOException xD)
			{
				;
			}
		}
	}
}
