package game;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import javax.swing.*;

public class Client {
	
	int board_size=0;
	/**
	 * wyniki gracza i przeciwnika
	 */
	int result = 0, opponentResult = 0;
	/**
	 * elementy interfejsu graficznego
	 */
	private JFrame frame = new JFrame("Go");
	private JFrame choice = new JFrame("Choice");
	private JLabel messageLabel = new JLabel("");
	private JLabel score1;
	private JLabel score2;
	private JButton button1;
	private JButton button2;
	/**
	 * obiekt drukujący na zewnątrz stringi
	 */
	PrintWriter out;
	
	/**
	 * Stworzenie ramki i wyświetlenie jej
	 */
	private void initFrame()
	{
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setResizable(false);
	}
	
	/**
	 * Tworzenie planszy gry
	 * @param size długość boku planszy
	 * @throws Exception
	 */
	private void setPanel(int size) throws Exception
	{
//		messageLabel.setBackground(Color.lightGray);
//		frame.getContentPane().add(messageLabel, "South");
		this.size = size;
		JPanel boardPanel = new JPanel();
		JPanel pan = new JPanel();
		JPanel buttons = new JPanel();
		pan.setLayout(new BorderLayout());
//		JLabel messages = new JLabel(" ");
		button1 = new JButton("Skip");
		button2 = new JButton("Surrender");
		score1 = new JLabel("You: 0");
		score2 = new JLabel("Foe: 0");
		pan.add(score1,BorderLayout.WEST);
		buttons.add(button1);
		buttons.add(button2);
		pan.add(buttons,BorderLayout.CENTER);
		pan.add(score2,BorderLayout.EAST);
		pan.add(messageLabel,BorderLayout.SOUTH);
		boardPanel.setBackground(Color.black);
		boardPanel.setLayout(new GridLayout(size,size));
		
		board = new Square[size][size];
		
		/**
		 * zdefiniowanie działań dla przycisków
		 */
		button1.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try {
					messageLabel.setText(" ");
					obj_output.writeObject(new Message(MessageType.BUTTON1));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		button2.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e)
			{
				try {
					obj_output.writeObject(new Message(MessageType.BUTTON2));
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		});
		
		for (int i=0; i<size; ++i)
			for (int j=0; j<size; ++j)
			{
				final int k=i, l=j;
				board[i][j] = new Square();
				board[i][j].addMouseListener(new MouseAdapter() {
					@Override
					public void mousePressed(MouseEvent m)
					{
						try{
							obj_output.writeObject(new Message(MessageType.MOVE, k*size+l));
						}
						catch (IOException xD)
						{
							System.exit(0);
						}
					}
				});
				board[i][j].setIcon(selectMove(FieldState.EMPTY, i*size+j));
				boardPanel.add(board[i][j]);
			}
			frame.getContentPane().add(boardPanel, "Center");
			frame.getContentPane().add(pan, "South");
	}
	
	/**
	 * Stworzenie graficznego interfejsu okna uruchamiającego
	 * @throws IOException
	 */
	private void setGui() throws IOException
	{
		out = new PrintWriter(socket.getOutputStream(), true);
		choice.setLayout(new FlowLayout());
		choice.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JButton playWithPlayer = new JButton("Dalej");
		JTextField nickname = new JTextField("Podaj swój nick");
		JTextField opponent = new JTextField("Podaj nick przeciwnika");

		score1 = new JLabel("You: 0");
		score2 = new JLabel("Foe: 0");
		
		choice.add(playWithPlayer);
		choice.add(nickname);
		choice.add(opponent);
		CheckboxGroup cbg = new CheckboxGroup();
		Checkbox x9 = new Checkbox("9",false,cbg);
		Checkbox x13 = new Checkbox("13",true,cbg);
		Checkbox x19 = new Checkbox("19",false,cbg);
		CheckboxGroup mode = new CheckboxGroup();
		Checkbox p = new Checkbox("Private", false,mode);
		Checkbox a = new Checkbox("AI", false, mode);
		Checkbox r = new Checkbox("Random", true, mode);
		choice.add(p);
		choice.add(a);
		choice.add(r);

		
		
		choice.add(x9);
		choice.add(x13);
		choice.add(x19);
		
		choice.pack();
		playWithPlayer.addActionListener(new ActionListener() {
			   @Override
			public void actionPerformed(ActionEvent e) {
				   initFrame();
				   String s;
				   String m;
				   if (x9.getState()) {s = "9 "; board_size=9;}
				   else if (x13.getState()){ s = "13";board_size=13;}
				   else {s = "19";board_size=19;}
				   if(p.getState()) {m = "p";}
				   else if(a.getState()) m = "a";
				   else m = "r";
				   out.println(s+m+nickname.getText());
				   if (p.getState())
					   out.println(opponent.getText());
				   try {
					setPanel(board_size);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				   initFrame();
				   choice.setState(Frame.ICONIFIED);
			   }
		});
		choice.setVisible(true);
	}
	

	/**
	 * Plansza - dwuwymiarowa tablica pól
	 */
	private Square[][] board;
	/**
	 * rozmiar planszy
	 */
	private int size;
	
	private static int PORT = 9000;
	private Socket socket;
	/**
	 * strumienie, którymi przesyłane będą wiadomości
	 */
	private ObjectInputStream obj_input;
	private ObjectOutputStream obj_output;
	
	public Client(String serverAddress) throws Exception {
		socket = new Socket(serverAddress, PORT);
		obj_input = new ObjectInputStream(socket.getInputStream());
		obj_output = new ObjectOutputStream(socket.getOutputStream());
		
		
	}
	
	/**
	 * Wybór płytki do wyświetlenia na podstawie położenia i stanu pola
	 * @param fs stan pola
	 * @param l położenie pola
	 * @return
	 * @throws Exception
	 */
	private String selectMove(FieldState fs, int l) throws Exception
	{		
//		System.out.println("selecting"+"data/"+FieldState.toString(fs)+"_"+locationToString(l)+".png");
		return "data/"+FieldState.toString(fs)+"_"+locationToString(l)+".png";
	}
	
	/**
	 * Opisanie względnego położenia pola na podstawie numeru płytki
	 * @param l	numer płytki
	 * @return względne położenie pola w formie słownej
	 * @throws Exception
	 */
	private String locationToString(int l) throws Exception
	{
		int i = l/size, j = l%size;
		
		if (i==0)
		{
			if (j==0)
				return "NW";
			else if (j==size-1)
				return "NE";
			else
				return "N";
		}
		else if (i==size-1)
		{
			if (j==0)
				return "SW";
			else if (j==size-1)
				return "SE";
			else
				return "S";
		}
		
		if (j==0)
			return "W";
		else if (j==size-1)
			return "E";
		
		
		if (size == 19)
		{
			if (( i==3 || i==9 || i==15 ) && ( j==3 || j==9 || j==15 ))
				return "DOT";
		}
		else if (size == 13)
		{
			if ((( i==2 || i==10 ) && ( j==2 || j==10 )) || (i==6 && j==6))
				return "DOT";
		}
		else if (size == 9)
		{
			if ((( i==2 || i==6 ) && ( j==2 || j==6 )) || (i==4 && j==4 ))
				return "DOT";
		}
		else
		{
			throw new Exception();
		}
		
		return "INTERIOR";
	}
	
	/**
	 * Pętla gry; czyta wiadomości i podejmuje działania w zależności od nich
	 * @throws Exception
	 */
	public void play() throws Exception
	{
		Message message;
//		System.out.println("oznaczanie...");
		try{
			
			for (;;)
			{
				message  = (Message) obj_input.readObject();
				MessageType m = message.type;
				
//				System.out.println(m);
				
				if ( m == MessageType.WELCOME )
				{
					if (message.change == FieldState.BLACK)
					{
						frame.setTitle("Go - black player");
						opponentResult = 6;
						System.out.println("setting opponent result to 6");
						score2.setText("Foe: "+opponentResult);
					}
					else
					{
						frame.setTitle("Go - white player");
						result = 6;
						System.out.println("setting result to 6");
						score1.setText("You: "+result);
					}
				}
				else if (m == MessageType.VALID_MOVE)
				{
					
					messageLabel.setText("Valid move, please wait");
				}
				else if (m == MessageType.NOTIFY)
				{
//					System.out.println("oznaczanie...");
					board[(message.n)/size][(message.n)%size].setIcon(selectMove(message.change, message.n)).repaint();
//					System.out.println("oznaczono");
				}
				else if (m == MessageType.VICTORY)
				{
					messageLabel.setText("You win" + (message.text == null ? "" : message.text));
//					break;
				}
				else if (m == MessageType.SCORED)
				{
					result = result + message.n;
					score1.setText("You: "+result);
				}
				else if (m == MessageType.OPPONENT_SCORED)
				{
					opponentResult = opponentResult + message.n;
					score2.setText("Foe: "+opponentResult);
				}
				else if (m == MessageType.DEFEAT)
				{
					messageLabel.setText("You lose");
//					break;
				}
				else if (m == MessageType.MESSAGE)
				{
					String ms = message.text;
					if (!ms.equals(""))
						messageLabel.setText(ms);
				}
				else if (m == MessageType.SHIFT)
				{
					if (message.n!=0)
					{
						button1.setEnabled(false);
						button2.setEnabled(false);
					}
					else if (button1.getText().equals("Skip"))
					{
						button1.setText("Break");
						button2.setText("Accept");
					}
					else
					{
						button1.setText("Skip");
						button2.setText("Surrender");
					}
				}
			}
//			obj_output.writeObject(new Message(MessageType.QUIT));
		}
		finally
		{
			socket.close();
		}
	}
	
//	private boolean wantsToPlayAgain()
//	{
//		int response = JOptionPane.showConfirmDialog(frame,
//	            "Want to play again?",
//	            "XDDDDD",
//	            JOptionPane.YES_NO_OPTION);
//	        frame.dispose();
//	        return response == JOptionPane.YES_OPTION;
//	}

	public static void main(String[] args) throws Exception {
//        while (true) {
		String serverAddress = (args.length == 0) ? "localhost" : args[1];
		Client client = new Client(serverAddress);
		client.setGui();
		client.play();
//            if (!client.wantsToPlayAgain()) {
//                break;
//            }
//        }
    }

}
