package game;
import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;


public class Server {

	BufferedReader in;
	private static HashSet<Player> players9 =new HashSet<Player>();
	private static HashSet<Player> players13 =new HashSet<Player>();
	private static HashSet<Player> players19 =new HashSet<Player>();
	
	private static Map<Player, String> players = new HashMap<Player, String>();
	
	/**
	 * łączenie graczy w pary; gracz wybiera grę z określonym przeciwnikiem i wtedy
	 * wpisuje jego nick, albo wybiera grę z losowym przeciwnikiem - wtedy łączony jest0
	 * z pierwszym-lepszym przeciwnikiem.
	 * Gra obsługuje 3 rozmiary planszy: 9x9, 13x13 i 19x19
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		ServerSocket listener = new ServerSocket(9000);
		System.out.println("System is running...");
		try{
			for (;;)
			{
				Socket socket = listener.accept();
				Player playerX = new Player(socket);
				BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				String temp = in.readLine();
				int rozmiar;
				String name;
				String mode;
				
				if(temp.startsWith("9"))
				{
					rozmiar=Integer.parseInt(temp.substring(0, 1));
					
					mode = temp.substring(2,3);
					name = temp.substring(3);
				}
				else
				{
					rozmiar=Integer.parseInt(temp.substring(0, 2));
					mode = temp.substring(2,3);
					name = temp.substring(3);
				}
				
				if(mode.equals("a"))
				{
					Game game = new Game(rozmiar);
					playerX.setMark(FieldState.BLACK);
					playerX.setGame(game);
					Bot bot = new Bot(FieldState.WHITE);
					bot.setGame(game);
					playerX.setOpponent(bot);
					bot.setOpponent(playerX);
					playerX.start();
					bot.start();
				}
				else if(mode.equals("r"))
				{
					Player playerO = null;
					if(rozmiar==9)
					{
						for(Player p : players9){
							if (players.get(p).equals("ANY"))
							{
								playerO = p;
								players9.remove(p);
								break;
							}
						}
					}
					else if(rozmiar==13)
					{
						for(Player p : players13){
							if (players.get(p).equals("ANY"))
							{
								playerO = p;
								players13.remove(p);
								break;
							}
						}
					}
					else if(rozmiar==19)
					{
						for(Player p : players19){
							if (players.get(p).equals("ANY"))
							{
								playerO = p;
								players19.remove(p);
								break;
							}
						}
					}
					if(playerO!=null)
					{
						if (playerO==playerX)
							System.out.println("ten sam gracz");
						Game game = new Game(rozmiar);
						playerX.setMark(FieldState.WHITE);
						playerO.setMark(FieldState.BLACK);
						playerX.setGame(game);
						playerO.setGame(game);
						game.currentPlayer = playerO;
						playerX.setOpponent(playerO);
						playerO.setOpponent(playerX);
						playerX.start();
						playerO.start();
						players.remove(playerO);
						players.remove(playerX);
					}
					else {
						System.out.println("dodawanie...");
						if(rozmiar==9){
							players9.add(playerX);
						}
						else if (rozmiar==13) {
							players13.add(playerX);
						}
						else if (rozmiar==19) {
							players19.add(playerX);
						}
						players.put(playerX, "ANY");
					}
					
				}
				else if (mode.equals("p"))
				{
					Player playerO = null;
//					System.out.println("xD");
					String opponent_name = in.readLine();
//					System.out.println(opponent_name);
					if(rozmiar==9)
					{
						for(Player p : players9){
							if (players.get(p).equals(opponent_name))
							{
								playerO = p;
								players9.remove(p);
								break;
							}
						}
					}
					else if(rozmiar==13)
					{
						for(Player p : players13){
							if (players.get(p).equals(opponent_name))
							{
								playerO = p;
								players13.remove(p);
								break;
							}
						}
					}
					else if(rozmiar==19)
					{
						for(Player p : players19){
							if (players.get(p).equals(opponent_name))
							{
								playerO = p;
								players19.remove(p);
								break;
							}
						}
					}
					if(playerO!=null)
					{
						Game game = new Game(rozmiar);
						playerX.setMark(FieldState.WHITE);
						playerO.setMark(FieldState.BLACK);
						playerX.setGame(game);
						playerO.setGame(game);
						game.currentPlayer = playerO;
						playerX.setOpponent(playerO);
						playerO.setOpponent(playerX);
						playerX.start();
						playerO.start();
						players.remove(playerO);
						players.remove(playerX);
					}
					else {
						if(rozmiar==9){
							players9.add(playerX);
						}
						else if (rozmiar==13) {
							players13.add(playerX);
						}
						else if (rozmiar==19) {
							players19.add(playerX);
						}
						players.put(playerX, name);
					}
				}
			}
		}
		finally
		{
			listener.close();
		}
	}
	
}

