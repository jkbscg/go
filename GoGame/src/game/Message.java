package game;

import java.io.Serializable;


public class Message implements Serializable {
	
	private static final long serialVersionUID = -7973872420754829427L;
	
	/**
	 * parametry, które mogą być przydatnymi elementami wiadomości
	 */
	public MessageType type;
	public String text;
	public int n;
	public FieldState change;
	
	public Message(MessageType m, String s, int n)
	{
		this.type = m;
		this.text = s;
		this.n = n;
	}
	
	public Message(MessageType m, String s)
	{
		this.type = m;
		this.text = s;
		this.n = 0;
	}
	
	public Message(MessageType m, int n)
	{
		this.type = m;
		this.text = "";
		this.n = n;
	}
	
	public Message(MessageType m, int n, FieldState fs)
	{
		this.type = m;
		this.n = n;
		this.change = fs;
	}
	
	public Message(MessageType m)
	{
		this.type = m;
		this.text = "";
		this.n = 0;
	}
}
