package game;


public class Field {

	/**
	 * stan pola (białe, czarne, puste, terytorium białego, terytorium czarnego)
	 */
	public FieldState state;
	/**
	 * zmienna wykorzystywana przy działaniach rekurencyjnych, mówiąca, czy pole zostało już odwiedzone
	 */
	public boolean checked;
	
	/**
	 * czy na polu nie ma pionu?
	 * @param fs stan pola
	 * @return
	 */
	public static boolean isEmpty(Field fs)
	{
		return fs.state == FieldState.EMPTY || fs.state == FieldState.WHITE_TERR || fs.state == FieldState.BLACK_TERR;
	}
	
	public Field()
	{
		state = FieldState.EMPTY;
		checked = false;
	}
	
	public Field(Field f)
	{
		state = f.state;
		checked = f.checked;
	}
	
	public void markChecked()
	{
		checked = true;
	}
}
