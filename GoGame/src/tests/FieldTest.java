package tests;
import game.*;
import junit.framework.TestCase;

public class FieldTest extends TestCase {
	public void testisEmpty(){
		Field s = new Field();
		Field bField = new Field();
		Field wField = new Field();
		s.state=FieldState.WHITE;
		bField.state= FieldState.BLACK;
		wField.state= FieldState.BLACK_TERR;
		assertEquals(Field.isEmpty(s), false);
		assertEquals(Field.isEmpty(bField), false);
		assertEquals(Field.isEmpty(wField), true);
	}
}
