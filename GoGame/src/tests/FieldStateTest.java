package tests;
import game.*;
import junit.framework.TestCase;

public class FieldStateTest extends TestCase {
	
	public void testgetOpponent(){
		final FieldState fs = null;
		final FieldState fs1 = FieldState.BLACK;
		final FieldState fs2 = FieldState.WHITE;
		assertEquals(FieldState.getOpponent(fs), FieldState.DUMMY);
		assertEquals(FieldState.getOpponent(fs1), FieldState.WHITE);
		assertEquals(FieldState.getOpponent(fs2), FieldState.BLACK);
	}
	public void testtoString(){
		final FieldState fs = FieldState.WHITE;
		final FieldState fs1 = FieldState.BLACK_TERR;
		final FieldState fs2 = FieldState.WHITE_TERR;
		final FieldState fs3 = FieldState.BLACK;
		assertEquals(FieldState.toString(fs), "WHITE");
		assertEquals(FieldState.toString(fs1), "BLACK_TERR");
		assertEquals(FieldState.toString(fs2), "WHITE_TERR");
		assertEquals(FieldState.toString(fs3), "BLACK");
	}
	public void testisEmpty() {
		final FieldState fs = FieldState.WHITE;
		final FieldState fs1 = FieldState.BLACK_TERR;
		final FieldState fs2 = FieldState.WHITE_TERR;
		final FieldState fs3 = FieldState.BLACK;
		assertEquals(FieldState.isEmpty(fs), false);
		assertEquals(FieldState.isEmpty(fs1), true);
		assertEquals(FieldState.isEmpty(fs2), true);
		assertEquals(FieldState.isEmpty(fs3), false);
	}
	public void testgetTeritorry() {
		final FieldState fs = FieldState.WHITE;
		final FieldState fs1 = FieldState.BLACK;
		final FieldState fs2 = FieldState.BLACK_TERR;
		assertEquals(FieldState.getTerritory(fs), FieldState.WHITE_TERR);
		assertEquals(FieldState.getTerritory(fs1), FieldState.BLACK_TERR);
		assertEquals(FieldState.getTerritory(fs2), FieldState.DUMMY);
		
	}
}

